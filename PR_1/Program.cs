﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PR_1.Task1;
using PR_1.Task2;
using PR_1.Task3;
using PR_1.Task4;

namespace PR_1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Task1
            int count = 10;
            var firstVector = new Vector(count, count * -1);
            var secondVector = new Vector(count / 2);
            var thirdVector = new Vector(count);
            var rand = new Random();
            Console.Write("First vector: ");
            for (int i = 0; i < count; i++)
            {
                firstVector[i - count] = rand.Next(1, count * 2);
                Console.Write("{0} ", firstVector[i - count]);
                thirdVector[i] = firstVector[i - count];
            }
            Console.WriteLine("\nIs firstVector equal to secondVector? {0}", firstVector == secondVector);
            Console.WriteLine("Is firstVector equal to thirdVector? {0}", firstVector == thirdVector);
            secondVector = firstVector + thirdVector;
            Console.Write("firstVector + thirdVector: ");
            for (int i = 0; i < count; i++)
            {
                Console.Write("{0} ", secondVector[i]);
            }
            secondVector = firstVector - thirdVector;
            Console.Write("\nfirstVector - thirdVector: ");
            for (int i = 0; i < count; i++)
            {
                Console.Write("{0} ", secondVector[i]);
            }
            firstVector.MultipleOnScalar(2);
            Console.Write("\nfirstVector after nultiply on 2: ");
            for (int i = 0; i < count; i++)
            {
                Console.Write("{0} ", firstVector[i - count]);
            }
            Console.Write("\n\n\n\n");
            #endregion

            #region Task2
            Console.Write("\n\n\n\n");
            Rectangle firstRect = new Rectangle(new Point(0, 5), new Size(3, 5));
            Rectangle secondRect = new Rectangle(new Point(5, 5), new Size(3, 5));
            Console.WriteLine("First recttangle\nStart point:({0}, {1}); Width: {2}; Height: {3}", firstRect.StartPoint.X, firstRect.StartPoint.Y, firstRect.Width, firstRect.Height);
            Console.WriteLine("Second recttangle\nStart point:({0}, {1}); Width: {2}; Height: {3}", secondRect.StartPoint.X, secondRect.StartPoint.Y, secondRect.Width, secondRect.Height);
            Console.WriteLine("Trying to make new rectangle with meeting this two rectangles...");
            try
            {
                Rectangle tempRect = new Rectangle(firstRect, secondRect);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Moving first rectangle: 3 to right and 2 to up");
            firstRect.Move(3, 2);
            Console.WriteLine("New first recttangle\nStart point:({0}, {1}); Width: {2}; Height: {3}", firstRect.StartPoint.X, firstRect.StartPoint.Y, firstRect.Width, firstRect.Height);
            Console.WriteLine("Trying again...");
            Rectangle thirdRect = new Rectangle(firstRect, secondRect);
            Console.WriteLine("Third recttangle\nStart point:({0}, {1}); Width: {2}; Height: {3}", thirdRect.StartPoint.X, thirdRect.StartPoint.Y, thirdRect.Width, thirdRect.Height);
            thirdRect.Scale(2);
            Console.WriteLine("Third recttangle after scaling on 2\nStart point:({0}, {1}); Width: {2}; Height: {3}", thirdRect.StartPoint.X, thirdRect.StartPoint.Y, thirdRect.Width, thirdRect.Height);
            Rectangle forthRect = new Rectangle(firstRect, thirdRect, InputType.Horizontal);
            Console.WriteLine("Rectangle for first and third rectangles");
            Console.WriteLine("Forth recttangle\nStart point:({0}, {1}); Width: {2}; Height: {3}", forthRect.StartPoint.X, forthRect.StartPoint.Y, forthRect.Width, forthRect.Height);
            Console.Write("\n\n\n\n");
            #endregion

            #region Task3
            Console.Write("\n\n\n\n");
            Polynome firstPolynome = new Polynome();
            //float[] asd = new float[3];
            //asd[0] = 1;
            //asd[1] = 2;
            //asd[2] = 3;
            //Polynome secondPolynome = new Polynome(asd, 2);
            Polynome secondPolynome = new Polynome(3);
            Console.WriteLine("A");
            Console.WriteLine(firstPolynome.ToString());
            Console.WriteLine("B");
            Console.WriteLine(secondPolynome.ToString());
            Console.WriteLine("A + B");
            Console.WriteLine((firstPolynome + secondPolynome).ToString());
            Console.WriteLine("A - B");
            Console.WriteLine((firstPolynome - secondPolynome).ToString());
            Console.WriteLine("B - A");
            Console.WriteLine((secondPolynome - firstPolynome).ToString());
            Console.WriteLine("A * B");
            Console.WriteLine((firstPolynome * secondPolynome).ToString());
            Console.Write("\n\n\n\n");
            #endregion

            #region Task4
            Console.Write("\n\n\n\n");
            Matrix firstMatrix = new Matrix(5, 5);
            Matrix secondMatrix;
            Console.WriteLine("First matrix");
            for(int i = 0; i < firstMatrix.RowCount; i++)
            {
                for(int j = 0; j < firstMatrix.ColumnCount; j++)
                {
                    firstMatrix[i][j] = rand.Next(1, 50);
                    Console.Write("{0, -10}", firstMatrix[i][j]);
                }
                Console.WriteLine();
            }
            secondMatrix = new Matrix(3, 5);
            Console.WriteLine("Second matrix");
            for (int i = 0; i < secondMatrix.RowCount; i++)
            {
                for (int j = 0; j < secondMatrix.ColumnCount; j++)
                {
                    secondMatrix[i][j] = rand.Next(1, 50);
                    Console.Write("{0, -10}", secondMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("First matrix + Second Matrix");
            try
            {
                var errorMatrix = firstMatrix + secondMatrix;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            secondMatrix = new Matrix(5, 5);
            Console.WriteLine("Second matrix");
            for (int i = 0; i < secondMatrix.RowCount; i++)
            {
                for (int j = 0; j < secondMatrix.ColumnCount; j++)
                {
                    secondMatrix[i][j] = rand.Next(1, 50);
                    Console.Write("{0, -10}", secondMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("FirstMatrix + SecondMatrix");
            var tempMatrix = firstMatrix + secondMatrix;
            for (int i = 0; i < tempMatrix.RowCount; i++)
            {
                for (int j = 0; j < tempMatrix.ColumnCount; j++)
                {
                    Console.Write("{0, -10}", tempMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("FirstMatrix - SecondMatrix");
            tempMatrix = firstMatrix - secondMatrix;
            for (int i = 0; i < tempMatrix.RowCount; i++)
            {
                for (int j = 0; j < tempMatrix.ColumnCount; j++)
                {
                    Console.Write("{0, -10}", tempMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("FirstMatrix * SecondMatrix");
            tempMatrix = firstMatrix * secondMatrix;
            for (int i = 0; i < tempMatrix.RowCount; i++)
            {
                for (int j = 0; j < tempMatrix.ColumnCount; j++)
                { 
                    Console.Write("{0, -10}", tempMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("First matrix minor(0, 0)");
            tempMatrix = new Matrix(tempMatrix.GetMinorTable(0));
            for (int i = 0; i < tempMatrix.RowCount; i++)
            {
                for (int j = 0; j < tempMatrix.ColumnCount; j++)
                {
                    Console.Write("{0, -10}", tempMatrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Determinant of this minor: {0}", Matrix.CalculateDeterminant(tempMatrix.Array));
            Console.Write("\n\n\n\n");
            #endregion

            Console.Read();
        }
    }
}
