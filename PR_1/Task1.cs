﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_1.Task1
{
    public class Vector
    {
        #region Fields

        private int[] _array;
        public int this[int index]
        {
            set
            {
                if (index - _startIndex >= 0 && index - _startIndex <= _count - 1)
                {
                    _array[index - _startIndex] = value;
                }
                else
                {
                    throw new Exception("Index out of bounds!");
                }
            }
            get
            {
                if(index - _startIndex >= 0 && index - _startIndex <= _count - 1)
                {
                    return _array[index - _startIndex];
                }
                throw new Exception("Index out of bounds!");
            }
        }

        private int _startIndex;
        public int StartIndex
        {
            get
            {
                return _startIndex;
            }
        }

        private int _count;
        public int Count
        {
            get
            {
                return _count;
            }
        }


        #endregion

        #region Methods

        public void MultipleOnScalar(int scalar)
        {
            for(int i = 0; i < _count; i++)
            {
                _array[i] *= scalar;
            }
        }

        #endregion

        #region OperatorsOverloading

        public static Vector operator +(Vector firstVector, Vector secondVector)
        {
            var count = firstVector.Count;
            if (count == secondVector.Count)
            {
                var tempVector = new Vector(count);
                var firstStartIndex = firstVector.StartIndex;
                var secondStartIndex = secondVector.StartIndex;
                for (int i = 0; i < count; i++)
                {
                    tempVector[i] = firstVector[firstStartIndex + i] + secondVector[secondStartIndex + i];
                }
                return tempVector;
            }
            throw new Exception("Vectors have different lengths!");
        }

        public static Vector operator -(Vector firstVector, Vector secondVector)
        {
            var count = firstVector.Count;
            if (count == secondVector.Count)
            {
                var tempVector = new Vector(count);
                var firstStartIndex = firstVector.StartIndex;
                var secondStartIndex = secondVector.StartIndex;
                for (int i = 0; i < count; i++)
                {
                    tempVector[i] = firstVector[firstStartIndex + i] - secondVector[secondStartIndex + i];
                }
                return tempVector;
            }
            throw new Exception("Vectors have different lengths!");
        }

        public static bool operator ==(Vector firstVector, Vector secondVector)
        {
            if (firstVector.Equals(null) && secondVector.Equals(null))
            {
                return true;
            }
            if(firstVector.Equals(null) || secondVector.Equals(null))
            {
                return false;
            }
            if (firstVector.Count != secondVector.Count)
            {
                return false;
            }
            var count = firstVector.Count;
            var firstStartIndex = firstVector.StartIndex;
            var secondStartIndex = secondVector.StartIndex;
            for (int i = 0; i < count; i++)
            {
                if (firstVector[firstStartIndex + i] != secondVector[secondStartIndex + i])
                {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(Vector firstVector, Vector secondVector)
        {
            return !(firstVector == secondVector);
        }

        #endregion

        #region Constructors

        public Vector(): this(10, 0)
        {

        }

        public Vector(int count): this(count, 0)
        {

        }

        public Vector(int count, int startIndex)
        {
            if (count <= 0)
            {
                count = 10;
            }
            _startIndex = startIndex;
            _count = count;
            _array = new int[count];
        }

        public Vector(int[] array):this(array, 0)
        {
            
        }

        public Vector(int[] array, int startIndex)
        {
            if (array != null)
            {
                if (array.Count() <= 0)
                {
                    throw new Exception("Input array is empty!");
                }
                _startIndex = startIndex;
                _count = array.Count();
                _array = new int[_count];
                for (int i = 0; i < _count; i++)
                {
                    _array[i] = array[i];
                }
            }
        }

        #endregion
    }
}
