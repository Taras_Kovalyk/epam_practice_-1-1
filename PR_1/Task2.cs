﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_1.Task2
{
    public struct Point
    {
        public int X;
        public int Y;
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    public struct Size
    {
        public float Width;
        public float Height;
        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    public enum InputType
    {
        Vertical,
        Horizontal
    }

    public class Rectangle
    {
        #region Fields

        private Point _startPoint;
        public Point StartPoint
        {
            set
            {
                _startPoint = StartPoint;
            }
            get
            {
                return _startPoint;
            }
        }

        private Size _size;
        public float Width
        {
            set
            {
                _size.Width = value;
            }
            get
            {
                return _size.Width;
            }
        }
        public float Height
        {
            set
            {
                _size.Height = value;
            }
            get
            {
                return _size.Height;
            }
        }


        #endregion

        #region Methods

        public Point Move(int x, int y)
        {
            _startPoint.X += x;
            _startPoint.Y += y;
            return _startPoint;
        }

        public Size Scale(float coefficient)
        {
            _size.Width *= coefficient;
            _size.Height *= coefficient;
            return _size;
        }

        #endregion

        #region Constructors

        public Rectangle():this(0, 0, 1, 1)
        {

        }

        public Rectangle(Point startPoint, Size size)
        {
            _startPoint = startPoint;
            _size = size;
        }

        public Rectangle(int x, int y, float width, float height)
        {
            _startPoint = new Point() { X = x, Y = y };
            _size = new Size() { Width = width, Height = height };
        }

        //Построить прямогульник, который является пересечением 2-х заданных
        public Rectangle(Rectangle firstRectangle, Rectangle secondRectangle)
        {
            if((secondRectangle._startPoint.X >= firstRectangle._startPoint.X) && (secondRectangle._startPoint.X <= firstRectangle._startPoint.X + firstRectangle._size.Width))
            {
                _startPoint.X = secondRectangle._startPoint.X;
                _size.Width = Math.Abs(secondRectangle._startPoint.X - (firstRectangle._startPoint.X + firstRectangle._size.Width));
            }
            else
            {
                if((secondRectangle._startPoint.X < firstRectangle._startPoint.X) && (firstRectangle._startPoint.X <= secondRectangle._startPoint.X + secondRectangle._size.Width))
                {
                    _startPoint.X = firstRectangle._startPoint.X;
                    _size.Width = Math.Abs(firstRectangle._startPoint.X - (secondRectangle._startPoint.X + secondRectangle._size.Width));
                }
                else
                {
                    throw new Exception("Rectangles don't meet!!!");
                }
            }
            if ((secondRectangle._startPoint.Y <= firstRectangle._startPoint.Y) && (secondRectangle._startPoint.Y >= firstRectangle._startPoint.Y - firstRectangle._size.Height))
            {
                _startPoint.Y = secondRectangle._startPoint.Y;
                _size.Height = Math.Abs(secondRectangle._startPoint.Y - (firstRectangle._startPoint.Y - firstRectangle._size.Height));
            }
            else
            {
                if ((secondRectangle._startPoint.Y > firstRectangle._startPoint.Y) && (firstRectangle._startPoint.Y >= secondRectangle._startPoint.Y - secondRectangle._size.Height))
                {
                    _startPoint.Y = firstRectangle._startPoint.Y;
                    _size.Height = Math.Abs(firstRectangle._startPoint.Y - (secondRectangle._startPoint.Y - secondRectangle._size.Height));
                }
                else
                {
                    throw new Exception("Rectangles don't meet!!!");
                }
            }
        }

        //Построить минимальный прямоугольник, чтобы вмещал 2 заданных(начальной точнкой выбирается начальная точка первого прямоугольника)
        public Rectangle(Rectangle firstRectangle, Rectangle secondRectangle, InputType type):this(firstRectangle, secondRectangle, type, firstRectangle._startPoint)
        {

        }

        //Построить минимальный прямоугольник, чтобы вмещал 2 заданных(начальная точка задаётся)
        public Rectangle(Rectangle firstRectangle, Rectangle secondRectangle, InputType type, Point startPoint)
        {
            if(firstRectangle == null || secondRectangle == null)
            {
                throw new Exception("Some of the rectangles is null!!!");
            }
            _startPoint = startPoint;
            switch(type)
            {
                case InputType.Horizontal:
                    {
                        _size.Width = firstRectangle._size.Width + secondRectangle._size.Width;
                        _size.Height = Math.Max(firstRectangle._size.Height, secondRectangle._size.Height);
                        break;
                    }
                case InputType.Vertical:
                    {
                        _size.Height = firstRectangle._size.Height + secondRectangle._size.Height;
                        _size.Width = Math.Max(firstRectangle._size.Width, secondRectangle._size.Width);
                        break;
                    }
                default:
                    {
                        throw new Exception("Unknown type!!!");
                    }
            }
        }

        #endregion
    }
}
