﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_1.Task3
{
    public class Polynome
    {
        #region Fields

        private float[] _coefficients;
        private int _rank;

        #endregion

        #region Methods

        public float Calculate(float x)
        {
            var count = _coefficients.Count();
            var temp = 0.0f;
            for (int i = 0; i < count; i++)
            {
                temp += (float)(Math.Pow(x, _rank - i) * _coefficients[i]);
            }
            temp += _coefficients[_rank];
            return temp;
        }

        public override string ToString()
        {
            string temp = "Polynome: ";
            for(int i = 0; i < _rank; i++)
            {
                temp += (_coefficients[i].ToString() + "x^" + (_rank - i) + " + ");
            }
            temp += _coefficients[_coefficients.Count() - 1];
            return temp;
        }

        #endregion

        #region OperatorsOverloading

        public static Polynome operator +(Polynome a, Polynome b)
        {
            float[] tempArray;
            if(a._rank > b._rank)
            {
                tempArray = new float[a._rank + 1];
                for(int i = 0; i <= a._rank; i++)
                {
                    tempArray[i] = a._coefficients[i];
                }
                for(int i = 0; i <= b._rank; i++)
                {
                    tempArray[i + (a._rank - b._rank)] += b._coefficients[i];
                }
            }
            else
            {
                tempArray = new float[b._rank + 1];
                for (int i = 0; i <= b._rank; i++)
                {
                    tempArray[i] = b._coefficients[i];
                }
                for (int i = 0; i <= a._rank; i++)
                {
                    tempArray[i + (b._rank - a._rank)] += a._coefficients[i];
                }
            }
            return new Polynome(tempArray, tempArray.Count() - 1);
        }

        public static Polynome operator -(Polynome a, Polynome b)
        {
            float[] tempArray;
            if (a._rank > b._rank)
            {
                tempArray = new float[a._rank + 1];
                for (int i = 0; i <= a._rank; i++)
                {
                    tempArray[i] = a._coefficients[i];
                }
                for (int i = 0; i <= b._rank; i++)
                {
                    tempArray[i + (a._rank - b._rank)] -= b._coefficients[i];
                }
            }
            else
            {
                tempArray = new float[b._rank + 1];
                for (int i = 0; i <= b._rank; i++)
                {
                    tempArray[i] = b._coefficients[i] * (-1);
                }
                for (int i = 0; i <= a._rank; i++)
                {
                    tempArray[i + (b._rank - a._rank)] += a._coefficients[i];
                }
            }
            return new Polynome(tempArray, tempArray.Count() - 1);
        }

        public static Polynome operator *(Polynome a, Polynome b)
        {
            int tempRank = a._rank + b._rank;
            float[] tempArray = new float[tempRank + 1];
            int countA = a._coefficients.Count();
            int countB = b._coefficients.Count();
            for(int i = 0; i < countA; i++)
            {
                for(int j = 0; j < countB; j++)
                {
                    tempArray[tempRank - (a._rank - i + b._rank - j)] += (a._coefficients[i] * b._coefficients[j]);
                }
            }
            return new Polynome(tempArray, tempArray.Count() - 1);
        }

        #endregion

        #region Constructors

        public Polynome():this(5)
        {
            
        }

        public Polynome(int rank)
        {
            var rand = new Random();
            _rank = rank;
            _coefficients = new float[_rank + 1];
            for (int i = 0; i <= _rank; i++)
            {
                _coefficients[i] = rand.Next(1, 5);
            }
        }

        public Polynome(float[] coefficients, int rank)
        {
            if(coefficients.Count() != rank + 1)
            {
                throw new Exception("Invalid length of coefficients array!!!");
            }
            if(rank > 10 || rank < 0)
            {
                throw new Exception("Invalid rank!!! Must be from 0 to 10.");
            }
            _coefficients = coefficients;
            _rank = rank;
        }

        #endregion
    }
}
