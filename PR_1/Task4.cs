﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_1.Task4
{
    public class Matrix
    {
        #region Fields

        private float[][] _array;
        public float[][] Array
        {
            get
            {
                return _array;
            }
        }
        public float[] this[int row]
        {
            set
            {
                _array[row] = value;
            }
            get
            {
                return _array[row];
            }
        }
        public float this[int row, int col]
        {
            set
            {
                _array[row][col] = value;
            }
            get
            {
                return _array[row][col];
            }
        }

        private int _rowCount;
        public int RowCount
        {
            get
            {
                return _rowCount;
            }
        }

        private int _colCount;
        public int ColumnCount
        {
            get
            {
                return _colCount;
            }
        }

        private int _maximumMinorRank;

        #endregion

        #region Methods

        public static float CalculateDeterminant(float[][] matrix)
        {
            if (matrix != null)
            {
                var rowCount = matrix.Count();
                var colCount = 0;
                for (int i = 0; i < rowCount; i++)
                {
                    if (matrix[i] == null)
                    {
                        throw new Exception((i + 1).ToString() + " row is null!");
                    }
                }
                colCount = matrix[0].Count();
                for (int i = 1; i < rowCount; i++)
                {
                    if (matrix[i].Count() != colCount)
                    {
                        throw new Exception("Rows have different lenght!");
                    }
                }
                if (rowCount != colCount)
                {
                    throw new Exception("RowCount != colCount");
                }
                if(colCount == 2)
                {
                    return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
                }
                float result = 0;
                for(int i = 0; i < rowCount; i++)
                {
                    result += matrix[0][i] * CalculateDeterminant(GetAdditionalMinor(matrix, 0, i)) * (float)(Math.Pow(-1, i % 2));
                }
                return result;
            }
            throw new Exception("Matrix is null!");
        }

        //Дополнительный минор(таблица)
        public static float[][] GetAdditionalMinor(float[][] matrix, int i, int j)
        {
            if(matrix != null)
            {
                var rowCount = matrix.Count();
                var colCount = 0;
                for (int o = 0; o < rowCount; o++)
                {
                    if (matrix[i] == null)
                    {
                        throw new Exception((i + 1).ToString() + " row is null!");
                    }
                }
                colCount = matrix[0].Count();
                if(i < 0 || i > rowCount || j < 0 || j > colCount)
                {
                    throw new Exception("Invalid input!");
                }
                for (int o = 1; o < rowCount; o++)
                {
                    if (matrix[o].Count() != colCount)
                    {
                        throw new Exception("Rows have different lenght!");
                    }
                }
                float[][] tempMatrix = new float[rowCount - 1][];
                for(int o = 0; o < rowCount - 1; o++)
                {
                    tempMatrix[o] = new float[colCount - 1];
                }
                for(int o = 0; o < i; o++)
                {
                    for(int k = 0; k < j; k++)
                    {
                        tempMatrix[o][k] = matrix[o][k];
                    }
                    for(int k = j + 1; k < colCount; k++)
                    {
                        tempMatrix[o][k - 1] = matrix[o][k];
                    }
                }
                for (int o = i + 1; o < rowCount; o++)
                {
                    for (int k = 0; k < j; k++)
                    {
                        tempMatrix[o - 1][k] = matrix[o][k];
                    }
                    for (int k = j + 1; k < colCount; k++)
                    {
                        tempMatrix[o - 1][k - 1] = matrix[o][k];
                    }
                }
                return tempMatrix;
            }
            throw new Exception("Matrix is null!");
        }

        //Минор k-го порядка(таблица)
        public float[][] GetMinorTable(int index)
        {
            return GetAdditionalMinor(_array, index, index);
        }

        //Минор k-го порядка(определитель)
        public float GetMinor(int index)
        {
            return CalculateDeterminant(GetAdditionalMinor(_array, index, index));
        }

        #endregion

        #region OperatorsOverloading
        public static Matrix operator +(Matrix firstMatrix, Matrix secondMatrix)
        {
            if(firstMatrix._rowCount != secondMatrix._rowCount || firstMatrix._colCount != secondMatrix._colCount)
            {
                throw new Exception("Matrices have different size!!!");
            }
            var tempMatrix = new Matrix(firstMatrix._rowCount, firstMatrix._colCount);
            for(int i = 0; i < tempMatrix._rowCount; i++)
            {
                for(int j = 0; j < tempMatrix._colCount; j++)
                {
                    tempMatrix[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
                }
            }
            return tempMatrix;
        }

        public static Matrix operator -(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix._rowCount != secondMatrix._rowCount || firstMatrix._colCount != secondMatrix._colCount)
            {
                throw new Exception("Matrices have different size!!!");
            }
            var tempMatrix = new Matrix(firstMatrix._rowCount, firstMatrix._colCount);
            for (int i = 0; i < tempMatrix._rowCount; i++)
            {
                for (int j = 0; j < tempMatrix._colCount; j++)
                {
                    tempMatrix[i][j] = firstMatrix[i][j] - secondMatrix[i][j];
                }
            }
            return tempMatrix;
        }

        public static Matrix operator *(Matrix firstMatrix, Matrix secondMatrix)
        {
            if (firstMatrix._rowCount != secondMatrix._colCount || firstMatrix._colCount != secondMatrix._colCount)
            {
                throw new Exception("Matrices have different size!!!");
            }
            var tempMatrix = new Matrix(firstMatrix._rowCount, firstMatrix._colCount);
            for (int i = 0; i < tempMatrix._rowCount; i++)
            {
                for (int j = 0; j < tempMatrix._colCount; j++)
                {
                    tempMatrix[i][j] = 0;
                    for(int o = 0; o < firstMatrix._colCount; o++)
                    {
                        tempMatrix[i][j] += firstMatrix[i][o] * secondMatrix[o][j];
                    }
                }
            }
            return tempMatrix;
        }
        #endregion

        #region Constructors

        public Matrix():this(5,5)
        {

        }

        public Matrix(int rowCount, int colCount)
        {
            if(rowCount <= 0)
            {
                rowCount = 5;
            }
            if(colCount <= 0)
            {
                colCount = 5;
            }
            _rowCount = rowCount;
            _colCount = colCount;
            if(_rowCount > _colCount)
            {
                _maximumMinorRank = _colCount;
            }
            else
            {
                _maximumMinorRank = _rowCount;
            }
            _array = new float[_rowCount][];
            for(int i = 0; i < _rowCount; i++)
            {
                _array[i] = new float[_colCount];
            }
        }

        public Matrix(float[][] matrix)
        {
            var rowCount = matrix.Count();
            var colCount = matrix[0].Count();
            for (int o = 1; o < rowCount; o++)
            {
                if (matrix[o].Count() != colCount)
                {
                    throw new Exception("Rows have different lenght!");
                }
            }
            _rowCount = rowCount;
            _colCount = colCount;
            _array = new float[_rowCount][];
            for(int i = 0; i < _rowCount; i++)
            {
                _array[i] = new float[_colCount];
                for(int j = 0; j < colCount; j++)
                {
                    _array[i][j] = matrix[i][j];
                }
            }
        }
        #endregion
    }
}
